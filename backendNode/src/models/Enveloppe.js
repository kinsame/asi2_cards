class Enveloppe {
    static getType() { return "fr.cpe.enveloppe.common.dto.EnveloppeDto" }

    constructor({object, objectType}) {
        if (!objectType && !object.constructor.getType) {
            throw new Error(`ERROR : objet ${object} is not compatible (objectType missing)`);
        }

        this.object = object;
        this.objectType = objectType || object.constructor.getType();
    }
}

module.exports = Enveloppe;