class Message {
    static getType() { return "fr.cpe.enveloppe.app1.dto.Message" }

    constructor({channelID, text, user, time}) {
        this.channelID = channelID;
        this.text = text;
        this.user = user;
        this.time = time;
    }
}

module.exports = Message;