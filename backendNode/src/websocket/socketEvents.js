const MessageSending = require('./../services/MessageSending');
exports = module.exports = function(io) {
  

  let userGameQueue = [];
  let matchmaking = [];

  var userConnected = 0;
  var onlineUsers = [];
  

  io.on("connection", socket => {
    console.log(`Socket ${socket.id} connected.`);
    socket.join("global");

    socket.on("nouveau_client", function(pseudo) {
      console.log(`nouveau client: ${pseudo}`);
      console.log(socket.id);
      socket.emit("nouveau_client", { pseudo: pseudo, id: socket.id });
    });

    socket.on("message", function(message) {
      console.log("Channel ID:",message.channelID, "    Message:", message.text);
      io.to(message.channelID).emit("message", message);
      MessageSending.run(message.text);
    });

    socket.on("join room", function(room) {
      console.log("join room" + room);
      socket.join(room);
    });

    socket.on("leave room", function(room) {
      console.log("leave room" + room);
      socket.leave(room);
    });

    socket.on("typing", function(user) {
      console.log(`typing`);
      socket.broadcast.emit("typing", {
        id: socket.id,
        user: user.user,
        channel: user.channel
      });
    });
    socket.on("stop typing", function(user) {
      console.log(`stop typing`);
      socket.broadcast.emit("stop typing", {
        id: socket.id,
        user: user.user,
        channel: user.channel
      });
    });

    /////////////////////////////////////////////
    // game Logic
    //////////////////////////////////////////////

    //join game socket if number of user >= 2 lauch the game
    socket.on("join game", function(userInfo) {
      console.log(`user join Game`);
      const people = {
        user: userInfo.user,
        cards: userInfo.cards,
        id: socket.id
      };

      userGameQueue.push(people);
      if (userGameQueue.length >= 2) {
        const gameId = Math.floor(Math.random() * 30000);
        const match = {
          gameId: gameId,
          user1Id: userGameQueue[0].id,
          user2Id: userGameQueue[1].id,
          userTurn: 1
        };
        console.log(`Launch Game`);
        console.log(userGameQueue)
        for (let i = 0; i < userGameQueue.length; i++) {
          if (i === 0) {
            console.log("launch game user 1")
            io.to(userGameQueue[i].id).emit("launch game", {
              gameId: gameId,
              user: userGameQueue[1].user,
              myCards: userGameQueue[i].cards,
              opponentCard: userGameQueue[1].cards
            });
          } else if (i === 1) {
            console.log("launch game user 2")
            io.to(userGameQueue[i].id).emit("launch game", {
              gameId: gameId,
              user: userGameQueue[0].user,
              myCards: userGameQueue[i].cards,
              opponentCard: userGameQueue[0].cards
            });
          }
        }
        // reset queue
        matchmaking.push(match);
        userGameQueue.splice(0, 2);
      }
    });

    socket.on("play attack turn", function(turn) {
      const match = matchmaking.filter(match => match.gameId === turn.id);
      const currentMatch =match[0]
      if (
        (currentMatch.userTurn === 1 && socket.id === currentMatch.user1Id) ||
        (currentMatch.userTurn === 2 && socket.id === currentMatch.user2Id)
      ) {
        // you turn
        console.log("your turn");
        // send attack 

        // send loose energy
      } else {
        // not you turn
        console.log("not your turn");
        //socket.emit("not your turn", "it's not your turn")
      }
    });

    socket.on("end turn", function(turn) {
      const match = matchmaking.filter(match => match.gameId === turn.id);
      const currentMatch =match[0]
      if (
        (currentMatch.userTurn === 1 && socket.id === currentMatch.user1Id) ||
        (currentMatch.userTurn === 2 && socket.id === currentMatch.user2Id)
      ) {
        if(currentMatch.userTurn === 1){
          currentMatch.userTurn = 2;
          io.to(currentMatch.user2Id).emit("end turn opponent", "your opponent finish his turn");
        }else{
          currentMatch.userTurn = 1;
          io.to(currentMatch.user1Id).emit("end turn opponent", "your opponent finish his turn");
        }
      } else {
        // not your turn
      }
    });

    socket.on("leave queue", function() {
      console.log("leave queue");
      console.log(socket.id);
      console.log(userGameQueue);
      userGameQueue = userGameQueue.filter(item => item.id !== socket.id);
      console.log(userGameQueue);
    });

    socket.on("select card", function(cards) {
      console.log(`launch game`);
      console.log(cardsse);
    });
  });
};
