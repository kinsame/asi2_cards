global.CONFIG = require('./config.json');
const cors = require('cors');
var app = require("express")();
var server = require("http").createServer(app);
const io = require("socket.io").listen(server)
const socketEvents = require('./src/websocket/socketEvents')(io);


// Chargement de la page index.html
app.get("/", function(req, res) {
  res.sendfile(__dirname + "/index.html");
  
});
server.listen(8080);