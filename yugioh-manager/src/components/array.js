import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import Card from "./card/card";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
}));



const header = [
  "name",
  "description",
  "family",
  "affinity",
  "energy",
  "hp",
  "price"
];


export default function SimpleTable({ cardList, handleClickLine, selectedLine }) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {header.map(item => (
              <TableCell>{item}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {cardList.map((row, index) => (
            <Card
              color={selectedLine === row.id ? "#f864" : "#fff0"}
              onClick={(card) => handleClickLine(row)}
              key={index}
              card ={row}
              type="text"
            />
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
