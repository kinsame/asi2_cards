import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

export default function SimpleCard({ onClick }) {
  const classes = useStyles();

  return (
    <Button
      onClick={onClick}
      variant="contained"
      color="secondary"
      className={classes.button}
    >
      End Turn
    </Button>
  );
}
