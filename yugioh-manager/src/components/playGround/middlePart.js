import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Plateau from "./plateau";
import { connect } from "react-redux";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

function MiddlePart({
  onSelectOCard,
  onSelectUCard,
  opponentCards,
  userCards
}) {
  const classes = useStyles();

  return (
    <Grid
      item
      xs={6}
      container
      direction="column"
      justify="center"
      alignItems="stretch"
    >
      <Plateau onClickCard={onSelectOCard} cards={opponentCards} />
      <Divider style={{ width: "90%" }} variant="middle" />
      <Plateau onClickCard={onSelectUCard} cards={userCards} />
    </Grid>
  );
}

export default MiddlePart;
