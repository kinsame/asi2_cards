import React from "react";
import { lighten, makeStyles, withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import KeyboardVoiceIcon from "@material-ui/icons/KeyboardVoice";
import LinearProgress from "@material-ui/core/LinearProgress";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: lighten("#ff6c5c", 0.5)
  },
  bar: {
    borderRadius: 20,
    backgroundColor: "#ff6c5c"
  }
})(LinearProgress);

export default function SimpleCard({ value }) {
  const classes = useStyles();

  return (
    <>
      Action point:
      <BorderLinearProgress
        color="secondary"
        variant="determinate"
        value={value}
      >test
      </BorderLinearProgress>
    </>
  );
}
