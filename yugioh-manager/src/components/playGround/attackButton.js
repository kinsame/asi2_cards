import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Gavel from "@material-ui/icons/Gavel";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

export default function SimpleCard({ onClick }) {
  const classes = useStyles();

  return (
    <Button
      onClick={onClick}
      variant="contained"
      color="secondary"
      className={classes.button}
      startIcon={<Gavel />}
    >
      Attack
    </Button>
  );
}
