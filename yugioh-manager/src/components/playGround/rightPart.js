import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import User from "../user/user";
import Card from "../card/card";
import AttackButton from "./attackButton";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

export default function RightPart({ onAttack, opponentCard, userCards }) {
  const classes = useStyles();

  return (
    <Grid
      item
      xs
      container
      direction="column"
      justify="space-between"
      alignItems="center"
    >
      <Grid item xs>
        <Card type="picture" card={opponentCard} size="large" />
      </Grid>
      <Grid item xs style={{ margin: "10px" }}>
        <AttackButton onClick={onAttack} />
      </Grid>
      <Grid item xs>
        <Card type="picture" card={userCards} size="large" />
      </Grid>
    </Grid>
  );
}
