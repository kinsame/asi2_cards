import React from "react";
import User from "../user/user"

const ChannelHeader = () => {
  return (
    <div>
      Chat
      <User name="John Doe" type ="avatar"/>
    </div>
  );
};

export default ChannelHeader;