import React from "react";
import Avatar from "../user/avatar";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  messageUser: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "5px",
    marginBottom: "5px"
  },
  messageStranger: {
    display: "flex",
    justifyContent: "flex-start",
    margin: "5px 15px 5px 0px"
  },
  textUser: {
    borderRadius: "10px",
    marginLeft: "10px",
    padding: "5px",
    paddingLeft: "10px",
    paddingRight: "10px",
    backgroundColor:"#3ae",
    color:"#fff",
  },
  textStranger: {
    borderRadius: "10px",
    marginLeft: "10px",
    padding: "5px",
    paddingLeft: "10px",
    paddingRight: "10px",
    backgroundColor:"#eee",
    color:"#777",
  }
});

const Message = ({ message, user, name}) => {
  const classes = useStyles();

  let isSentByCurrentUser = false;

  if(user === name) {
    isSentByCurrentUser = true;
  }

  return (
    <div className={isSentByCurrentUser ? classes.messageUser : classes.messageStranger}>
      {!isSentByCurrentUser && <Avatar size="small" name={user} />}
      <span className={isSentByCurrentUser ? classes.textUser : classes.textStranger}>
        {message}
      </span>
    </div>
  );
};

export default Message;
