import React, { useState, useContext , useEffect} from "react";
import MessageList from "./messageList";
import SendMessageForm from "./sendMessageForm";
import Typing from "./typing";
import ChannelList from "./channelList";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import { Scrollbars } from "react-custom-scrollbars";

import SocketContext from "../../container/socketContext";
import { sendMessageToRoom, selectChannel } from "../../ducks/message";


const Chat = ({
  sendMessage,
  messageList,
  currentUser,
  typersList,
  selectChannel,
  currentChannel
}) => {
  const [message, setMessage] = useState("");
  const [typing, setTyping] = useState(false);

  const socket = useContext(SocketContext);


  useEffect(() => {
    socket.emit("nouveau_client", "user_template");
  },[]);

  console.log(socket)

  // on submit messaeg
  const handleSubmit = e => {
    e.preventDefault();

    const newMessage = {
      channelID: currentChannel,
      text: message,
      user: currentUser
    };

    if (message !== "" && message !== null && message !== undefined) {
      setTyping(false);
      socket.emit("message", newMessage);
      socket.emit("stop typing", {
        user: currentUser,
        channel: currentChannel
      });
    }
    setMessage("");
  };
  const handleChange = value => {
    setMessage(value);
    if (!typing) {
      setTyping(true);
      socket.emit("typing", { user: currentUser, channel: currentChannel });
    }
    if (value < 1 && typing) {
      setTyping(false);
      socket.emit("stop typing", {
        user: currentUser,
        channel: currentChannel
      });
    }
  };

  const handleChangeRoom = channel => {
    selectChannel(channel);
    socket.emit("join room", channel);
  };

  return (
    <Grid direction={"column"} justify={"space-between"} container>
      <Grid item xs>
        <ChannelList onSelect={handleChangeRoom} />
      </Grid>
      <Grid item xs>
        <Scrollbars autoHeight style={{ height: "100%" }}>
          <MessageList
            messageList={messageList[currentChannel]}
            name={currentUser}
          />
        </Scrollbars>
      </Grid>
      <Grid item xs>
        {typersList.length > 0 && (
          <Typing typers={typersList} name={currentUser} />
        )}
      </Grid>
      <Grid item xs>
        <SendMessageForm
          onSubmit={handleSubmit}
          setMessage={handleChange}
          message={message}
        />
      </Grid>
    </Grid>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    sendMessage: (message, isCurrent) =>
      dispatch(sendMessageToRoom(message, isCurrent)),
    selectChannel: channel => dispatch(selectChannel(channel))
  };
};

const mapStateToProps = state => ({
  messageList: state.chat.messages,
  currentChannel: state.chat.currentChannel,
  currentUser: state.chat.currentUser,
  typersList: state.typers.typerList,
  socket: state.socket.socket
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
