import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import ReactLoading from "react-loading";
import Avatar from "../user/avatar";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  chip: {
    marginLeft:"5px",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#eee",
    borderRadius: "25px",
    width: "55px"
  }
});

const Typing = ({ isTyping, typers, name }) => {
  const classes = useStyles();

  const newTypers = typers.filter(typer => typer.user !== name);

  return (
    <div className={classes.container}>
      {newTypers.length > 0 && (
        <>
          {newTypers.map(typer => (
              <Avatar size="tiny" name={typer.user} />
            ))}
          <Chip
            className={classes.chip}
            avatar={
              <ReactLoading
                type={"bubbles"}
                color={"#aaa"}
                height={"100%"}
                width={"100%"}
              />
            }
          ></Chip>
        </>
      )}
    </div>
  );
};

export default Typing;
