import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
}));

const currencies = [
  { label: "Global", value: "global" },
  { label: "General", value: "general" },
  { label: "Test", value: "test" }
];

export default function ChannelList({ onSelect }) {
  const classes = useStyles(); 

  const handleChange= (e) =>{
    onSelect(e.target.value)
  }

  return (
    <TextField
      fullWidth
      id="standard-select-currency-native"
      select
      onChange={handleChange}
      className={classes.textField}
      SelectProps={{
        native: true,
        MenuProps: {
          className: classes.menu
        }
      }}
      margin="normal"
    >
      {currencies.map(option => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </TextField>
  );
}
