import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { Link } from "react-router-dom";
import ScreenLabel from "../constants/screenLabel";
import { ShoppingCart, SportsEsports, AttachMoney } from "@material-ui/icons";

const drawerWidth = 180;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },

  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  }
}));

const itemSidebar = [
  {
    label: "Sell",
    icon: <AttachMoney />,
    url: ScreenLabel.SELL
  },
  {
    label: "Buy",
    icon: <ShoppingCart />,
    url: ScreenLabel.BUY
  },
  {
    label: "Game",
    icon: <SportsEsports />,
    url: ScreenLabel.MATCH_MAKING
  }
];

export default function Sidebar({ open }) {
  const classes = useStyles();
  const theme = useTheme();

  const SidebarItem = ({ label, icon, url }) => {
    return (
      <ListItem component={Link} to={url} button>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    );
  };

  return (
    <div className={classes.root}>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
        open={open}
      >
        <div className={classes.toolbar} />
        <List>
          {itemSidebar.map((item, index) => (
            <SidebarItem
              key={index}
              url={item.url}
              label={item.label}
              icon={item.icon}
            />
          ))}
        </List>
      </Drawer>
    </div>
  );
}
