import React, { Component } from 'react';

class Balance extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);        
    }
  
    
  //render function use to update the virtual dom
  render() {
      return (
        <h2>{this.props.value}$ </h2>
        );
  }
}

//export the current classes in order to be used outside
export default Balance;