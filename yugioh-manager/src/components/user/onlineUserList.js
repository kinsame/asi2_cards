import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import { Paper } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import User from "../user/user";
import IconButton from "@material-ui/core/IconButton";
import CommentIcon from "@material-ui/icons/Comment";

const useStyles = makeStyles({
  avatar: { display: "flex", flexDirection: "row" }
});

const currentUser = {
  id: 5,
  name: "Doe",
  surname: "John"
};

export default function OnlineUserList({}) {
  const classes = useStyles();

  const _renderAvatar = () => {
    return <User name={"John Doe"} type="avatar" size="small" />;
  };

  const _renderEmpty = () => {
    return (
      <>
        <ListItemIcon>
          <Avatar className={classes.avatar}></Avatar>
        </ListItemIcon>
        <ListItemText primary={`Empty`} />
        <ListItemSecondaryAction>
          <CircularProgress className={classes.progress} size={20} />
        </ListItemSecondaryAction>
      </>
    );
  };

  const _renderProfile = user => {
    console.log(user);
    if (user !== undefined)
      return (
        <>
          <ListItemIcon>{_renderAvatar()}</ListItemIcon>
          <ListItemText primary={`${user.name} ${user.surname}`} />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="comments">
              <CommentIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </>
      );
    else return _renderEmpty();
  };

  return (
    <Grid item container>
      <Paper elevation={5}>
        <Typography variant="body" align="center">
          {`their are 5 player online`}
        </Typography>
        <ListItem role={undefined} dense button>
          {_renderProfile(currentUser)}
        </ListItem>
        <List className={classes.root} dense={true}>
          {[0, 1, 2, 3, 5, 6].map((item, index) => (
            <ListItem key={index} role={undefined} dense button>
              {_renderProfile(item)}
            </ListItem>
          ))}
        </List>
      </Paper>
    </Grid>
  );
}
