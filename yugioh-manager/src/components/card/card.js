import React from "react";
import PictureCard from "./pictureCard";
import CardArrayItem from "./cardArrayItem";

function Card({
  card,
  type,
  size,
  onClick,
  onClickCard,
  onClickDetail,
  color
}) {
  const drawCard = () => {
    if (type === "text") {
      return (
        <CardArrayItem
          id={card.id}
          name={card.name}
          description={card.description}
          family={card.family}
          affinity={card.affinity}
          energy={card.energy}
          hp={card.hp}
          price={card.price}
          onClick={onClick}
          color={color}
        />
      );
    } else if (type === "picture") {
      return (
        <PictureCard
          card={card}
          size ={size}
          onClickCard={onClickCard}
          onClickDetail={onClickDetail}
        />
      );
    }
  };

  return <>{drawCard()}</>;
}
export default Card;
