import React from "react";
import Button from "./button.js";
import CustomCard from "./card.js";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345
  },
  cardAction: {
    justifyContent: "center"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  }
}));

function InfoCard({ buttonLabel, OnButtonClick, card }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CustomCard
        type="picture"
        card ={card}
      />

      <CardActions className={classes.cardAction} disableSpacing>
        <Button label={buttonLabel} onClick={OnButtonClick} />
      </CardActions>
    </Card>
  );
}
export default InfoCard;
