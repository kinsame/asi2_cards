import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import {
  FlashOnOutlined,
  FavoriteBorder,
  GavelOutlined,
  SecurityOutlined,
  MoreHoriz
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import Badge from "@material-ui/core/Badge";

const useStyles = makeStyles(theme => ({
  paper: {
    maxHeight: "auto",
    maxWidth: "auto"
  },
  card: {
    margin: theme.spacing(0)
  },
  media: {
    height: 0,
    paddingTop: "150%" // 16:9
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  smallIcon: {
    fontSize: 15,
    color: "#f78"
  },
  largeIconHearth: {
    fontSize: 25,
    color: "#a42"
  },
  largeIconFlash: {
    fontSize: 25,
    color: "#aa2"
  }
}));

function PictureCard({ card, size, onClickCard, onClickDetail }) {
  const classes = useStyles();
  const [hover, sethover] = useState(false);

  const toggleHover = () => {
    sethover(!hover);
  };

  const renderCardContent = () => {
    if (size === "large") {
      return (
        <div className={classes.card}>
          <Typography variant="body2" color="textSecondary" component="p">
            {card.description}
          </Typography>
          <div>
            <div className={classes.row}>
              <FavoriteBorder className={classes.smallIcon} />
              <Typography variant="body2" color="textSecondary" component="p">
                {`${Math.floor(card.hp)} hp`}
              </Typography>
              <FlashOnOutlined className={classes.smallIcon} />
              <Typography variant="body2" color="textSecondary" component="p">
                {`${Math.floor(card.energy)} energy`}
              </Typography>
            </div>
            <div className={classes.row}>
              <GavelOutlined className={classes.smallIcon} />
              <Typography variant="body2" color="textSecondary" component="p">
                {`${Math.floor(card.attack)} attack`}
              </Typography>
              <SecurityOutlined className={classes.smallIcon} />
              <Typography variant="body2" color="textSecondary" component="p">
                {`${Math.floor(card.defence)} defence`}
              </Typography>
            </div>
          </div>
        </div>
      );
    } else if (size === "small") {
      return (
        <div onClick={onClickDetail}>
          <MoreHoriz className={classes.smallIcon} />
        </div>
      );
    }
  };

  return (
    <Card
      onMouseEnter={toggleHover}
      onMouseLeave={toggleHover}
      onClick={onClickCard}
      className={classes.paper}
      elevation={hover && size === "small" ? 5 : 1}
    >
      {card !== null ? (
        <>
          <CardContent style={{ padding: "8px" }}>
            <div className={classes.row}>
              <div>
                <Badge
                  className={classes.margin}
                  badgeContent={Math.floor(card.hp)}
                  anchorOrigin={{
                    horizontal: "right",
                    vertical: "bottom"
                  }}
                  color="none"
                  max={10000}
                >
                  <FavoriteBorder className={classes.largeIconHearth} />
                </Badge>
              </div>
              <Typography
                variant="main2"
                noWrap
                color="textSecondary"
                component="p"
              >
                {card.name}
              </Typography>
              <div>
                <Badge
                  className={classes.margin}
                  badgeContent={Math.floor(card.energy)}
                  anchorOrigin={{
                    horizontal: "left",
                    vertical: "bottom"
                  }}
                  color="none"
                  max={10000}
                >
                  <FlashOnOutlined className={classes.largeIconFlash} />
                </Badge>
              </div>
            </div>
          </CardContent>
          <CardMedia
            className={classes.media}
            image={card.imgUrl}
            title="Card"
          />
          <CardContent style={{ padding: "12px" }}>
            {renderCardContent()}
          </CardContent>
        </>
      ) : (
        <div>emptycard</div>
      )}
    </Card>
  );
}
export default PictureCard;
