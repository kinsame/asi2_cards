import React from "react";
import Card from "./card";
import Grid from "@material-ui/core/Grid";

function CardsList({ cardsList, onSelectCard,onClickDetail }) {

  return (
    <Grid item container spacing={3}>
      {cardsList.map((card, index) => (
        <Grid item md={2} xs ={6} key={index}>
          <Card key={card.id} card={card} type="picture" size="small" onClickCard={() => onSelectCard(card)} onClickDetail={() => onClickDetail(card)} />
        </Grid>
      ))}
    </Grid>
  );
}
export default CardsList;
