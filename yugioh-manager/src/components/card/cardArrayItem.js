import React from "react";
import PictureCard from "./pictureCard";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

function CardArrayItem({
  id,
  name,
  description,
  family,
  affinity,
  energy,
  hp,
  price,
  color,
  onClick
}) {
  const handleClick = id => {
    onClick(id);
  };
  return (
    <TableRow
      style={{ backgroundColor: color }}
      hover
      onClick={() => handleClick(id)}
    >
      <TableCell align="left">{name}</TableCell>
      <TableCell align="left">{description}</TableCell>
      <TableCell align="left">{family}</TableCell>
      <TableCell align="left">{affinity}</TableCell>
      <TableCell align="left">{energy}</TableCell>
      <TableCell align="left">{hp}</TableCell>
      <TableCell align="left">{price}</TableCell>
    </TableRow>
  );
}
export default CardArrayItem;
