import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Card from "./card";
import Grid from "@material-ui/core/Grid";

import Typography from "@material-ui/core/Typography";
import { Paper } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  cardEmpty: {
    backgroundColor: "#f1f1f1",
    margin: theme.spacing(2),
    boxShadow: `0 0 0 4px ${"#f79"}`
  }
  ,
  paper:{
    padding: theme.spacing(2),
  }
}));

function CardsList({ cardsList,onClickDetail }) {
  const classes = useStyles();

  const renderCard = index => {
    const card = cardsList[index];

    if (card !== undefined)
      return (
         <Grid item xs>
          <Card key={card.id} card={card} type="picture" size="small" onClickDetail={() => onClickDetail(card)} />
        </Grid>
      );
    else
      return (
         <Grid item xs>
          <Paper className={classes.cardEmpty}>
            <Typography align="center">
              Drop
              <br /> Card
              <br /> Here!
            </Typography>
          </Paper>
        </Grid>
      );
  };

  return (
    <Grid item  spacing={1}>
      <Paper className={classes.paper} elevation={5}>
        <Typography variant="h6" align="center">
          Cards
        </Typography>
        {[0, 1, 2, 3].map(index => renderCard(index))}
      </Paper>
    </Grid>
  );
}
export default CardsList;
