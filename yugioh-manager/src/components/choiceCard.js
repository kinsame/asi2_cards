import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const useStyles = makeStyles({});

export default function SimpleCard({ label, icon, url }) {
  const classes = useStyles();

  return (
    <Button
      variant="contained"
      size="large"
      className={classes.button}
      startIcon={icon}
      component={Link}
      to={url}
    >
      {label}
    </Button>
  );
}
