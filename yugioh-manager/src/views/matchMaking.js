import React, { useState, useContext } from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Chat from "../components/chat/chat";
import CardsList from "../components/card/cardsList";
import CardsSelection from "../components/card/cardsSelection";
import QuickPlayButton from "../components/playGround/quickPlayButton";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";

import SocketContext from "../container/socketContext";

import Card from "../components/card/card";

import Typography from "@material-ui/core/Typography";
import ReactLoading from "react-loading";

import { fakeUserCards } from "../helper/fakeUserCards";

import {
  addCardMatchMaking,
  removeCardMatchMaking
} from "../ducks/matchMaking";

import Config from "../constants/backendConfig.json";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    justifyContent: "center",
    backgroundColor: "#eee",
    padding: theme.spacing(5)
  },
  modalButton: {
    margin: theme.spacing(1)
  },
  modalLoading: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "5%",
    height: "5%"
  }
}));

const MatchMaking = ({
  cardUserList,
  addCard,
  removeCard,
  cardMatchMaking,
  user
}) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [toogledetail, setToogledetail] = useState(false);
  const [selectedCard, setSelectedCard] = useState({});

  const socket = useContext(SocketContext);

  const handlePlay = () => {
    console.log(cardMatchMaking);

    const userInfo ={user:user, cards:cardMatchMaking}
    if (cardMatchMaking.length > 0) {
      socket.emit("join game", userInfo);
      setLoading(true);
    }
  };

  const handleSelectCard = card => {
    if (cardMatchMaking.length < 4) {
      console.log("ajout de carte");
      console.log(cardMatchMaking.length);

      addCard(card);
    }
  };
  const handleDetail = card => {
    console.log("draw detail please");
    setSelectedCard(card);
    setToogledetail(true);
  };

  const LeaveQueue = () => {
    socket.emit("leave queue", "");
    setLoading(false);
  };

  return (
    <Grid container spacing={1} wrap="nowrap" alignItems="stretch">
      <Grid
        item
        xs={2}
        container
        direction="column"
        justify="space-between"
        alignItems="center"
      >
        <Chat />
      </Grid>
      <Grid
        item
        xs={8}
        direction="column"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h5" align="center">
          Pick your cards
        </Typography>
        <CardsList
          cardsList={cardUserList}
          onSelectCard={handleSelectCard}
          onClickDetail={handleDetail}
        />
      </Grid>
      <Grid
        item
        xs={2}
        direction="column"
        justify="center"
        alignContent="center"
      >
        <Typography variant="h5" align="center">
          Your selection
        </Typography>
        <CardsSelection
          cardsList={cardMatchMaking}
          onClickDetail={handleDetail}
        />
        <QuickPlayButton onPlayButtonClick={handlePlay} />
      </Grid>
      <Modal
        disablePortal
        disableEnforceFocus
        disableAutoFocus
        open={loading}
        className={classes.modal}
        aria-labelledby="server-modal-title"
        aria-describedby="server-modal-description"
      >
        <div className={classes.modalLoading}>
          <ReactLoading type={"spinningBubbles"} color={"#ff4"} />
          <span>en attente d'un adveraire</span>
          <Button
            className={classes.modalButton}
            variant="contained"
            color="secondary"
            onClick={LeaveQueue}
          >
            Leave Queue
          </Button>
        </div>
      </Modal>
      <Modal
        open={toogledetail}
        onClose={() => setToogledetail(false)}
        className={classes.modal}
        aria-labelledby="server-modal-title"
        aria-describedby="server-modal-description"
      >
        <div>
          <Card card={selectedCard} type="picture" size="large" />
          <Button
            className={classes.modalButton}
            variant="contained"
            color="secondary"
            onClick={() => setToogledetail(false)}
          >
            Close
          </Button>
        </div>
      </Modal>
    </Grid>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addCard: card => dispatch(addCardMatchMaking(card)),
    removeCard: card => dispatch(removeCardMatchMaking(card))
  };
};

const mapStateToProps = state => ({
  user: state.user.user,
  cardUserList: state.cardUser.userCardList,
  cardMatchMaking: state.matchMaking.matchMakingCards
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MatchMaking);
