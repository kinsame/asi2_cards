import React from "react";
import User from "../components/user/user";
import CardChoice from "../components/choiceCard.js";
import Container from "@material-ui/core/Container";
import Header from "../components/header";

import { ShoppingCart, SportsEsports, AttachMoney } from "@material-ui/icons";

const Landing = () => {
  return (
    <Container maxWidth="false">
      <div
        style={{
          display: "flex",
          flexGrow: 1,
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        <CardChoice label={"label 1"} icon={<ShoppingCart />} url="/buy" />
        <CardChoice label={"label 2"} icon={<AttachMoney />} url="/sell" />
        <CardChoice label={"label 2"} icon={<SportsEsports />} url="/game" />
      </div>
    </Container>
  );
};

export default Landing;
