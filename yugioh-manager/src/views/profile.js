import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import CreateIcon from "@material-ui/icons/Create";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";

import { signUp } from "../api/springBackend/user";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      //backgroundImage: "linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)"
    }
  },
  gradiant: {},
  paper: {
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(3)
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#26E1BC"
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const intitialState = {
  email: "",
  LastName: "",
  login: "",
  Surname: "",
  pwd: "",
  pwdConfirm: ""
};

function SignIn({ history, setCurrentUser, user }) {
  const classes = useStyles();

  const [connexion, setConnexion] = useState(user);
  const [disabled, setDisabled] = useState(true);
  const [changePassword, setChangePassword] = useState(false);

  useEffect(() => {
    setConnexion(user);
  }, [user]);

  console.log(user);
  const handleSubmit = e => {
    e.preventDefault();
    // check if password = confirmed password

    if (connexion.pwd === connexion.pwdConfirm) {
      delete connexion.pwdConfirm;
      signUp(connexion);
      console.log(connexion);
    } else {
      alert("password are different");
    }
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setConnexion({ ...connexion, [name]: value });
  };

  const handleChangePassword = () => {
    setChangePassword(!changePassword);
  };

  const handleEdit = () => {
    setDisabled(!disabled);
  };

  return (
    <Container component="main" maxWidth="md">
      <div className={classes.gradiant}>
        <Paper className={classes.paper}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Typography component="h1" variant="h5">
              Profile
            </Typography>
            <IconButton onClick={handleEdit}>
              <CreateIcon />
            </IconButton>
          </div>
          <form className={classes.form} noValidate submit>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <TextField
                  disabled={disabled}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Login"
                  name="login"
                  autoComplete="login"
                  onChange={handleChange}
                  value={connexion.login}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  disabled={disabled}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Email"
                  name="email"
                  autoComplete="email"
                  onChange={handleChange}
                  value={connexion.email}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  disabled={disabled}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Surname"
                  name="surName"
                  autoComplete="surname"
                  onChange={handleChange}
                  value={connexion.surName}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  disabled={disabled}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="lastName"
                  name="LastName"
                  autoComplete="lastName"
                  onChange={handleChange}
                  value={connexion.lastName}
                />
              </Grid>
              <Button onClick={handleChangePassword}> Change password</Button>

              {changePassword && (
                <>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="oldPwd"
                      label="Old password"
                      type="password"
                      autoComplete="current-pwd"
                      onChange={handleChange}
                      value={connexion.oldPwd}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="newPwd"
                      label="New Password"
                      type="password"
                      autoComplete="current-pwd"
                      onChange={handleChange}
                      value={connexion.newPwd}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="pwdConfirm"
                      label="Confirmed Password"
                      type="password"
                      autoComplete="current-pwd"
                      onChange={handleChange}
                      value={connexion.newPwdConfirm}
                    />
                  </Grid>
                </>
              )}
            </Grid>
          </form>
        </Paper>
      </div>
    </Container>
  );
}

const mapStateToProps = state => ({
  user: state.user.user
});

const mapDispatchToProps = dispatch => {};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SignIn));
