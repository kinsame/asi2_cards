import React, { useState } from "react";

import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";

import Config from "../config/config.json";
import setAuthorizationToken from "../utils/authorizationHeader";
import { setCurrentUser } from "../ducks/auth";
import { loadUser } from "../ducks/currentUser";

import { loadCard } from "../ducks/currentUserCards";

import { authentification } from "../api/springBackend/user";
import { isNull } from "util";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      //backgroundImage: "linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)"
    }
  },
  gradiant: {},
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(3)
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#26E1BC"
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const initialValue = {
  surname: "",
  password: ""
};

function SignIn({ history, setCurrentUser, setUser, setUserCard }) {
  const classes = useStyles();

  const [connexion, setConnexion] = useState(initialValue);

  const handleSubmit = e => {
    e.preventDefault();
    const { login, password } = connexion;
    authentification(login, password).then(function(result) {
      var dataUser = result;
      if (dataUser != 0) {
        const accessToken = `RandomToken${dataUser[0].id}`;
        localStorage.setItem(Config.ACCESS_TOKEN, accessToken);
        setAuthorizationToken(accessToken);
        setCurrentUser(accessToken);

        delete dataUser.pwd;
        console.log(dataUser);
        console.log(dataUser.cardList);
        setUserCard(dataUser[0].cardList);
        setUser(dataUser[0]);
        history.push("/game");
      } else {
        alert("Mauvais identifiants, veuillez réessayer.");
      }
    });
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setConnexion({ ...connexion, [name]: value });
  };

  return (
    <Container component="main" maxWidth="md">
      <div className={classes.gradiant}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Surname"
              name="login"
              autoComplete="surname"
              onChange={handleChange}
              value={connexion.username}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              autoComplete="current-password"
              onChange={handleChange}
              value={connexion.password}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick={handleSubmit}
              className={classes.submit}
            >
              Connect
            </Button>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              component={Link}
              to="/signup"
            >
              Register
            </Button>
          </form>
        </Paper>
      </div>
    </Container>
  );
}

const mapDispatchToProps = dispatch => {
  //setCurrentUser,
  return {
    setCurrentUser: user => dispatch(setCurrentUser(user)),
    setUser: user => dispatch(loadUser(user)),
    setUserCard: cards => dispatch(loadCard(cards))
  };
};
export default connect(
  null,
  mapDispatchToProps
)(withRouter(SignIn));
