import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";

import { signUp } from "../api/springBackend/user";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      //backgroundImage: "linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)"
    }
  },
  gradiant: {},
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(3)
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#26E1BC"
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const intitialState = {
  email: "",
  LastName: "",
  login: "",
  Surname: "",
  pwd: "",
  pwdConfirm: ""
};

function SignIn({ history, setCurrentUser }) {
  const classes = useStyles();

  const [connexion, setConnexion] = useState(intitialState);

  const handleSubmit = e => {
    e.preventDefault();
    // check if password = confirmed password

    if (connexion.pwd === connexion.pwdConfirm) {
      delete connexion.pwdConfirm;
      signUp(connexion);
      console.log(connexion);
    } else {
      alert("password are different");
    }
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setConnexion({ ...connexion, [name]: value });
  };

  return (
    <Container component="main" maxWidth="md">
      <div className={classes.gradiant}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} noValidate submit>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Login"
                  name="login"
                  autoComplete="login"
                  onChange={handleChange}
                  value={connexion.login}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Email"
                  name="email"
                  autoComplete="email"
                  onChange={handleChange}
                  value={connexion.email}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="Surname"
                  name="surName"
                  autoComplete="surname"
                  onChange={handleChange}
                  value={connexion.surName}
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  label="LastName"
                  name="lastName"
                  autoComplete="lastName"
                  onChange={handleChange}
                  value={connexion.lastName}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="pwd"
                  label="Password"
                  type="password"
                  autoComplete="current-pwd"
                  onChange={handleChange}
                  value={connexion.pwd}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="pwdConfirm"
                  label="Confirmed Password"
                  type="password"
                  autoComplete="current-pwd"
                  onChange={handleChange}
                  value={connexion.pwdConfirm}
                />
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                onClick={handleSubmit}
                className={classes.submit}
              >
                sign up
              </Button>

              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                component={Link}
                to="/connexion"
              >
                Already have an account?
              </Button>
            </Grid>
          </form>
        </Paper>
      </div>
    </Container>
  );
}

const mapDispatchToProps = dispatch => {};
export default connect(
  null,
  mapDispatchToProps
)(withRouter(SignIn));
