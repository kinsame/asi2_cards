import React, { useState } from "react";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import SimpleArray from "../components/array.js";
import Button from "../components/card/button";
import Card from "../components/card/card";

import { addCardToStore } from "../ducks/currentStoreCards";
import { sellCard, selectCard } from "../ducks/currentUserCards";

import { addMoney } from "../ducks/currentUser";
import { sellCardRequest } from "../api/springBackend/card";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(3)
  },
  card: {
    justifyContent: "center"
  }
}));

const Sell = ({ cardUser, addCard, sellCard, selectCard, addMoney, user }) => {
  const classes = useStyles();

  const [selectedLine, setSelectedLine] = useState(null);
  const [selectedCard, setSelectedCard] = useState(cardUser.currentCard);

  const handleVisibility = card => {
    console.log(card);
    selectCard(card);
    setSelectedCard(card);
    setSelectedLine(card.id);
  };

  const handleSellButton = () => {
    sellCardRequest(selectedCard, user.user.id);
    addCard(selectedCard);
    sellCard(selectedCard.id);
    setSelectedCard({});
    addMoney(selectedCard.price);
  };

  const renderFooterCard = () => {
    if (
      selectedCard !== null &&
      selectedCard !== undefined &&
      Object.keys(selectedCard).length !== 0 &&
      selectedCard.constructor === Object
    )
      return (
        <div className={classes.card}>
          <Card type="picture" card={selectedCard} size="large" />
          <Button
            label={`Sell (${selectedCard.price} $)`}
            onClick={handleSellButton}
          />
        </div>
      );
  };

  return (
    <Container maxWidth="xl">
      <Grid className={classes.container} container spacing={3}>
        <Grid item xs>
          <SimpleArray
            cardList={cardUser.userCardList}
            selectedLine={selectedLine}
            handleClickLine={handleVisibility}
          />
        </Grid>
        <Grid item>{renderFooterCard()}</Grid>
      </Grid>
    </Container>
  );
};
const mapDispatchToProps = dispatch => {
  return {
    selectCard: card => dispatch(selectCard(card)),
    addCard: card => dispatch(addCardToStore(card)),
    sellCard: id => dispatch(sellCard(id)),
    addMoney: money => dispatch(addMoney(money))
  };
};

const mapStateToProps = state => ({
  cardUser: state.cardUser,
  user: state.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sell);
