import React, { useEffect, useContext } from "react";
import { connect } from "react-redux";

import { useHistory } from "react-router-dom";

import {
  addUserToRoom,
  sendMessageToRoom,
  setChatUser,
} from "../ducks/message/action-creators";

import { startTyping, stopTyping } from "../ducks/typers";
import { loadPlayground } from "../ducks/currentGameCards";

import SocketContext from "./socketContext";
import playGround from "../components/playGround/playGround";

const WebSocketContainer = ({
  sendMessage,
  setChatUser,
  startTyping,
  stopTyping,
  currentUser,
  loadPlayground
}) => {

  let history = useHistory();
  const socket = useContext(SocketContext)

  console.log(socket)

  useEffect(() => {
    const random = Math.round(Math.random() * 1000);
    const user = `Unamed${random}`;
    
    socket.emit("nouveau_client", user);
    setChatUser(user);

    // receive new message

    socket.on("typing", user => startTyping(user));
    socket.on("stop typing", user => stopTyping(user));

    socket.on("nouveau_client", message => {
      console.log("nouveau client");
      console.log(message);
    });

    socket.on("connected", user => {
      console.log(user);
    });

    socket.on("message", message => {
      console.log("message");
      console.log(message);
      sendMessage(message);
    });

    socket.on("launch game", playground => {
      console.log("Launch game")
      console.log(playground)
      loadPlayground(playground)
      history.push(`/game/${playground.gameId}`);
    });

    // typing
    socket.on("typing", user => {
      if (user.id !== currentUser.id) startTyping(user);
    });
    socket.on("stop typing", user => stopTyping(user));

    socket.on("end turn opponent", () => alert("opponent fisnish turn"));

    socket.emit("nouveau_client", user);
    setChatUser(user);

    return () => {
      socket.off();
    };
  }, []);

  return <div>{console.log(socket.id)}</div>;
};

const mapStateToProps = state => ({
  currentUser: state.chat.currentUser
});

const mapDispatchToProps = dispatch => {
  return {
    sendMessage: (message, isCurrent) =>
      dispatch(sendMessageToRoom(message, isCurrent)),
    addUserToRoom: user => dispatch(addUserToRoom(user)),
    setChatUser: user => dispatch(setChatUser(user)),
    startTyping: user => dispatch(startTyping(user)),
    stopTyping: user => dispatch(stopTyping(user)),
    loadPlayground: playGround => dispatch(loadPlayground(playGround))
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WebSocketContainer);
