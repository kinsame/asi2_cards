import React from "react";
import { Link } from "react-router";

export default function MainLayout({ children }) {
  return <div>{children}</div>;
}
