import {
  ADD_CARD_TO_MATCH_MAKING,
  REMOVE_CARD_TO_MACH_MAKING
} from "./action-types";

export function addCardMatchMaking(card) {
  console.log("Add card from store:" + card);
  return {
    type: ADD_CARD_TO_MATCH_MAKING,
    card: card
  };
}

export function removeCardMatchMaking(cardId) {
  return {
    type: REMOVE_CARD_TO_MACH_MAKING,
    cardId: cardId
  };
}
