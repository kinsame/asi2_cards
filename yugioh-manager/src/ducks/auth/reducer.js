import {SET_CURRENT_USER, DELETE_CURRENT_USER, SET_CURRENT_USER_ROLE} from './action-types.js';

const initialState = {
    isAuthenticated: false,
    user:{},
    role:''
};

export default (state = initialState, action ={}) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return{
                isAuthenticated: true,
                user: action.user
            }
        case DELETE_CURRENT_USER:
            return{
                isAuthenticated: false,
                user: action.user
            }
        case SET_CURRENT_USER_ROLE:
            return{
                ...state,
                role: action.role
            }
        default: return state;
    }
};
