import { START_TYPING, STOP_TYPING } from "./action-types";

export function startTyping(user) {
  return {
    type: START_TYPING,
    username: user
  };
}

export function stopTyping(user) {
  return {
    type: STOP_TYPING,
    username: user
  };
}
