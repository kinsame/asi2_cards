import {
  LOAD_USER,
  ADD_MONEY_TO_WALLET,
  DELETE_MONEY_TO_WALLET
} from "./action-types";

import { fakeStore } from "../../helper/fakeStore";

const initialState = {
  user: { name: "John", surname: "Doe" },
  wallet: 5000
};

export default (state = initialState, action = {}) => {
  let newMoney;
  switch (action.type) {
    case LOAD_USER:
      const newAccount = action.user.account;
      console.log(newAccount);
      return { ...state, user: action.user, wallet: newAccount };
    case ADD_MONEY_TO_WALLET:
      newMoney = state.wallet + action.money;
      return { ...state, wallet: newMoney };
    case DELETE_MONEY_TO_WALLET:
      newMoney = state.wallet - action.money;
      return { ...state, wallet: newMoney };
    default:
      return state;
  }
};
