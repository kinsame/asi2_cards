import {
  LOAD_USER,
  ADD_MONEY_TO_WALLET,
  DELETE_MONEY_TO_WALLET
} from "./action-types";

export function loadUser(user) {
  console.log("load user" + user);
  console.log(user)
  return {
    type: LOAD_USER,
    user: user
  };
}

export function addMoney(money) {
  console.log("Add money to store" + money);
  return {
    type: ADD_MONEY_TO_WALLET,
    money: money
  };
}

export function deleteMoney(money) {
  console.log("Delete money from store" + money);
  return {
    type: DELETE_MONEY_TO_WALLET,
    money: money
  };
}
