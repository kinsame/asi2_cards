import {
  SELECT_GAME_OPPONENT_CARD,
  SELECT_GAME_USER_CARD,
  LOAD_PLAYGROUND
} from "./action-types";

import {fakeUserCards} from "../../helper/fakeUserCards"
import {fakeStore} from "../../helper/fakeStore"

const initialState = {
  gameId:"",
  opponent:"Jean Michel",
  userCards:[],
  opponentCards: [],
  currentUserCard: null,
  currentOpponentCard: null
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SELECT_GAME_OPPONENT_CARD:
      return { ...state, currentOpponentCard: action.card };
    case SELECT_GAME_USER_CARD:
      return { ...state, currentUserCard: action.card };
    case LOAD_PLAYGROUND:
      return {
        ...state,
        gameId: action.playground.gameId,
        userCards: action.playground.myCards,
        opponentCards: action.playground.opponentCard,
        opponent:action.playground.user
      };
    default:
      return state;
  }
};
