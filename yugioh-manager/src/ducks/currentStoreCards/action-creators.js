import {
  ADD_CARD_TO_STORE,
  REMOVE_CARD_TO_STORE,
  LOAD_CARDS_TO_STORE,
  SELECT_CARD_STORE
} from "./action-types";

export function addCardToStore(card) {
  console.log("ac dd card du store" + card);
  return {
    type: ADD_CARD_TO_STORE,
    card: card
  };
}

export function removeCardToStore(id) {
  console.log("remove card du store" + id);
  return {
    type: REMOVE_CARD_TO_STORE,
    cardId: id
  };
}

export function loadCardToStore(cardList) {
  console.log("select card");

  console.log(cardList)
  return {
    type: LOAD_CARDS_TO_STORE,
    cards: cardList
  };
}

export function selectCard(card) {
  console.log("load card");
  return {
    type: SELECT_CARD_STORE,
    card: card
  };
}
