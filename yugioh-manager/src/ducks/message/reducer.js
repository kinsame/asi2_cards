import {
  SET_USER,
  ADD_USER,
  SEND_MESSAGE,
  SELECT_CHANNEL
} from "./action-types.js";

const initialState = {
  messages: { global: [], test: [], general: [] },
  currentChannel: "global",
  currentUser: { name: "", id: "" }
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        currentUser: action.user
      };
    case ADD_USER:
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case SELECT_CHANNEL:
      return {
        ...state,
        currentChannel: action.channel
      };
    case SEND_MESSAGE:
      return {
        ...state,
        messages: {
          ...state.messages,
          [action.message.channelID]: [
            ...state.messages[action.message.channelID],
            action.message
          ]
        }
      };
    default:
      return state;
  }
};
