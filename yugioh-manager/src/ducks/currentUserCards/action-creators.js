import { BUY_CARD, SELL_CARD, SELECT_CARD_USER, LOAD_CARD } from "./action-types";

export function buyCard(card) {
  console.log("Buy card" + card);
  return {
    type: BUY_CARD,
    card: card
  };
}

export function sellCard(id) {
  console.log("Sell card" + id);
  return {
    type: SELL_CARD,
    cardId: id
  };
}

export function selectCard(card) {
  console.log("Select card" + card);
  return {
    type: SELECT_CARD_USER,
    card: card
  };
}

export function loadCard(cardList) {
  console.log("Load card");
  console.log(cardList);
  return {
    type: LOAD_CARD,
    cards: cardList
  };
}
