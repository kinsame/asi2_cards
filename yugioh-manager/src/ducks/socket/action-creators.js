
import { INIT_SOCKET_IO} from './action-types'

export function initSocketIo(socket) {
    return {
        type: INIT_SOCKET_IO,
        socket :socket
    }
}





