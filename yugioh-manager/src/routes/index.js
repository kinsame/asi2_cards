import { Route, Switch, IndexRoute } from "react-router";

import MainLayout from "../layout/main";
import AuthenticatedLayout from "../layout/authenticatedLayout";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Connexion from "../views/connexion";
import Sell from "../views/sell";
import Buy from "../views/buy";
import Landing from "../views/landing";
import Game from "../views/game";
import MatchMaking from "../views/matchMaking";
import SignUp from "../views/signUp"
import Profile from "../views/profile";
import ScreenLabel from "../constants/screenLabel"
import WebSocketContainer from "../container/websocket";


const routes = [
  {
    path: ScreenLabel.SELL,
    component: Sell,
    exact: true
  },
  {
    path: ScreenLabel.BUY,
    component: Buy,
    exact: true
  },
  {
    path: ScreenLabel.GAME,
    component: Game,
    exact: true
  },
  {
    path: ScreenLabel.MATCH_MAKING,
    component: MatchMaking,
    exact: true
  },
  {
    path: ScreenLabel.PROFILE,
    component: Profile ,
    exact: true
  },
  {
    path: ScreenLabel.LANDING,
    component: Landing,
    exact: true
  }
];

export default function ApplicationRouter() {
 
  return (
    <Router component={Connexion}>
     <WebSocketContainer />
      <Switch>
        <Route path={ScreenLabel.CONNEXION} component={Connexion} />
        <Route path={ScreenLabel.SIGN_UP} component={SignUp} />
        {routes.map((route, index) => (
          <AuthenticatedLayout
            key={index}
            path={route.path}
            component={route.component}
            exact={routes.exact}
          />
        ))}
      </Switch>
    </Router>
  );
}
